package de.christopherolbertz.ajax.view;

/**
 * Diese Bean simuliert einen Kunden.
 */
public class Customer {
	// ********************************************************* Attribute *************************************************
	/**
	 * Die Kundennummer.
	 */
	private String id;
	/**
	 * Der Vorname des Kunden.
	 */
	private String firstName;
	/**
	 * Der Nachname des Kunden.
	 */
	private String lastName;
	/**
	 * Der Kontostand.
	 */
	private double balance;
	
	// ********************************************************* Konstruktoren **********************************************
	public Customer(String id, String firstName, String lastName, double balance) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.balance = balance;
	}
 
	// ********************************************************* Getter und Setter ******************************************
	public String getId(){
		return id;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}
}
