package de.christopherolbertz.ajax.view;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class ColorBean {
	private String selectedColor;

	public String getSelectedColor() {
		return selectedColor;
	}

	public void setSelectedColor(String selectedColor) {
		this.selectedColor = selectedColor;
	}
	
	public String getColor() {
		if (selectedColor == null) {
			return "Keine Farbe gewaehlt!";
		} else {
			return "Farbe: " + selectedColor;
		}
	}
	
	public String getColorStyle() {
		return "background-color: " + selectedColor;
	}
}
