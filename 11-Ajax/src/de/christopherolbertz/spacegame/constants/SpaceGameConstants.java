package de.christopherolbertz.spacegame.constants;

public class SpaceGameConstants {
	public static final int STANDARD_STOCKROOM = 100;
	public static final int STANDARD_CANON_COUNT = 1;
	public static final int STANDARD_SHIELDS = 1;
	public static final int STANDARD_SENSOR_COUNT = 1;
	public static final int STANDARD_CAMOUFLAGE = 1;
}
