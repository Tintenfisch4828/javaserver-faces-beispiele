package de.christopherolbertz.spacegame.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import de.christopherolbertz.spacegame.factories.ServiceFactory;
import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.service.interfaces.PlanetService;

@FacesConverter(forClass = Planet.class)
public class PlanetConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, 
							  UIComponent component, 
							  String value) {
		final PlanetService planetService = 
				ServiceFactory.createPlanetService();
		final Planet planet = planetService.
				findPlanetByName(value);
		return planet;
	}

	@Override
	public String getAsString(FacesContext context, 
							  UIComponent component, 
							  Object value) {
		final Planet planet = (Planet)value;
		return planet.getPlanetName();
	}

}
