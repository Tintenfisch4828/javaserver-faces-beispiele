package de.christopherolbertz.spacegame.view;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

import de.christopherolbertz.spacegame.model.classes.UserImpl;
import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.model.interfaces.Starship;
import de.christopherolbertz.spacegame.model.interfaces.User;
import de.christopherolbertz.spacegame.service.classes.PlanetServiceImpl;
import de.christopherolbertz.spacegame.service.classes.StarshipServiceImpl;
import de.christopherolbertz.spacegame.service.interfaces.PlanetService;
import de.christopherolbertz.spacegame.service.interfaces.StarshipService;
import de.christopherolbertz.spacegame.utils.JsfUtils;

@ManagedBean(name = "tactical")
@ViewScoped
public class TacticalOverview implements Serializable {
	private static final long serialVersionUID = -749195364844497033L;
	private List<Planet> myPlanets;
	private PlanetService planetService;
	private DataModel<Starship> myStarships;
	private StarshipService starshipService;
	private User user;
	private Starship currentStarship;
	private int currentStarshipIndex;
	private boolean dataNotChanged;
	private Planet selectedPlanet;

	@PostConstruct
	public void initialize() {
		starshipService = new StarshipServiceImpl();
		planetService = new PlanetServiceImpl();
		if (dataNotChanged == false) {
			user = (User)JsfUtils.getBeanAttribute("currentUser", "loginView", UserImpl.class);
			myPlanets = planetService.findPlanetsByUser(user);
			selectedPlanet = myPlanets.get(0);
			List<Starship> starshipsOnPlanet = starshipService.findAllStarshipsOnPlanet(myPlanets.get(0));
			myStarships = new ListDataModel<Starship>();						
			myStarships.setWrappedData(starshipsOnPlanet);
			dataNotChanged = true;
		}
	}
	
	public void changePlanet(ValueChangeEvent event) {
		selectedPlanet = (Planet)event.getNewValue();
		final List<Starship> allStarships = starshipService.
				findAllStarshipsOnPlanet(selectedPlanet); 
		myStarships.setWrappedData(allStarships);
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.addMessage(event.getComponent().getClientId(), 
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Aenderung", 
						"Planet wurde geaendert, Raumschiffe werden angezeigt!"));
	}
	
	/*public void initialize(ComponentSystemEvent event) {
		if (dataNotChanged == false) {
			user = (User)JsfUtils.getBeanAttribute("currentUser", "loginView", UserImpl.class);
			myPlanets = planetService.findPlanetsByUser(user);
			selectedPlanetId = String.valueOf(selectedPlanet.getPlanetId());
			List<Starship> starshipsOnPlanet = starshipService.findAllStarshipsOnPlanet(myPlanets.get(0));
			myStarships = new ListDataModel<Starship>();						
			myStarships.setWrappedData(starshipsOnPlanet);
			dataNotChanged = true;
		}
	}*/
	
	public void showStarship(ActionEvent event) {
		currentStarship = myStarships.getRowData();
		currentStarshipIndex = myStarships.getRowIndex();
	}
	
	public List<Planet> getMyPlanets() { 
		return myPlanets;
	}

	public void setMyPlanets(List<Planet> myPlanets) {
		this.myPlanets = myPlanets;
	}
	
	public User getUser() {
		return user;
	}

	public DataModel<Starship> getMyStarships() {
		return myStarships;
	}

	public void setMyStarships(DataModel<Starship> myStarships) {
		this.myStarships = myStarships;
	}

	public Starship getCurrentStarship() {
		return currentStarship;
	}

	public void setCurrentStarship(Starship currentStarship) {
		this.currentStarship = currentStarship;
	}

	public int getCurrentStarshipIndex() {
		return currentStarshipIndex;
	}

	public void setCurrentStarshipIndex(int currentStarshipIndex) {
		this.currentStarshipIndex = currentStarshipIndex;
	}

	public boolean isDataNotChanged() {
		return dataNotChanged;
	}

	public void setDataNotChanged(boolean dataNotChanged) {
		this.dataNotChanged = dataNotChanged;
	}
	
	public Planet getSelectedPlanet() {
		return selectedPlanet;
	}
	
	public void setSelectedPlanet(Planet selectedPlanet) {
		this.selectedPlanet = selectedPlanet;
	}
}
