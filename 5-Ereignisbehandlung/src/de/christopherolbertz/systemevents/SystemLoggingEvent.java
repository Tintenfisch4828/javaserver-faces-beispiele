package de.christopherolbertz.systemevents;

import javax.faces.application.Application;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.PostConstructApplicationEvent;
import javax.faces.event.PreDestroyApplicationEvent;
import javax.faces.event.SystemEvent;
import javax.faces.event.SystemEventListener;

public class SystemLoggingEvent implements SystemEventListener {

	@Override
	public boolean isListenerForSource(Object source) {
		return (source instanceof Application);
	}

	@Override
	public void processEvent(SystemEvent systemEvent) throws AbortProcessingException {
		if (systemEvent instanceof PostConstructApplicationEvent) {
			System.out.println("******* PostConstructApplicationEvent *******");
		} else if (systemEvent instanceof PreDestroyApplicationEvent) {
			System.out.println("******* PreDestroyApplicationEvent *******");
		}
	}

}
