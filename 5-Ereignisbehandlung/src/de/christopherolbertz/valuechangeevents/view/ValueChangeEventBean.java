package de.christopherolbertz.valuechangeevents.view;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;

@ManagedBean
@ViewScoped
public class ValueChangeEventBean {
	private String inputText;
	private String outputText;
	
	public void onTextChanged(ValueChangeEvent valueChangeEvent) {
		outputText = "Der Text wurde geändert!";
		
		final String oldValue = (String)valueChangeEvent.getOldValue();
		final String newValue = (String)valueChangeEvent.getNewValue();
		
		if (oldValue == null) {
			outputText = outputText + " Der alte Text war leer";
		} else {
			outputText = outputText + " Der alte Text war: " + oldValue;
		}
		
		outputText = outputText + " Neuer Text: " + newValue;
	}
	
	public String getInputText() {
		return inputText;
	}

	public void setInputText(String inputText) {
		this.inputText = inputText;
	}
	
	public String getOutputText() {
		return outputText;
	}
	
	public void setOutputText(String outputText) {
		this.outputText = outputText;
	}
}
