package de.christopherolbertz.valuechangeevents.view;

/**
 * Berechnet die Teilersumme einer Zahl.
 * @author christopher
 *
 */
public class Math {
	public static int calculateFactorSum(int n) {
		
		int factorSum = 0;
		try {
			for (int i = 1; i <= n; i++) {
				if(n % i == 0) {
					factorSum = factorSum + i;
					// Damit es richtig lange dauert.
					Thread.sleep(1000);
				}
			}
		} catch (InterruptedException e) {
			// einfach ignorieren
		}
		
		return factorSum;
	}
}
