package de.christopherolbertz.valuechangeevents.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.model.SelectItem;

@ManagedBean
@ViewScoped
public class SelectOneView {
	private List<SelectItem> countries;
	private List<Language> languages;
	private String selectedCountry;
	private String selectedGender;
	private String selectedLanguage;

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	public String getSelectedLanguage() {
		return selectedLanguage;
	}

	public void setSelectedLanguage(String selectedLanguage) {
		this.selectedLanguage = selectedLanguage;
	}

	public void initialize(ComponentSystemEvent event) {
		countries = new ArrayList<>();
		countries.add(new SelectItem("DE", "Deutschland"));
		countries.add(new SelectItem("FR", "Frankreich"));
		countries.add(new SelectItem("ES", "Spanien"));
		
		final Language german = new Language("de", "deutsch");
		final Language french = new Language("fr", "französisch");
		final Language spanish = new Language("es", "spanisch");
		languages = new ArrayList<>();
		languages.add(german);
		languages.add(french);
		languages.add(spanish);
		
	}

	public String getSelectedGender() {
		return selectedGender;
	}

	public void setSelectedGender(String selectedGender) {
		this.selectedGender = selectedGender;
	}

	public List<SelectItem> getCountries() {
		return countries;
	}

	public void setCountries(List<SelectItem> countries) {
		this.countries = countries;
	}

	public String getSelectedCountry() {
		return selectedCountry;
	}

	public void setSelectedCountry(String selectedCountry) {
		this.selectedCountry = selectedCountry;
	}
}
