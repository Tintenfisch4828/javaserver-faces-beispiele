package de.christopherolbertz.valuechangeevents.view;

import java.math.BigInteger; 
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

/**
 * Diese Bean enthaelt die Listener fuer die Ansicht der Berechnung der Teilersumme.
 * @author christopher
 *
 */
@ManagedBean
@SessionScoped
public class FactorSumView {
	private static final int N_MAX = 1000;
	private int value;
	private int factorSum;
	
	public void onValueChanged(ValueChangeEvent valueChangeEvent) {
		int newValue = (Integer)valueChangeEvent.getNewValue();
		factorSum = Math.calculateFactorSum(newValue);
	}
	
	/**
	 * Liste von SelectItems mit den moeglichen Werten fuer die Berechnung
	 * erzeugen.
	 */
	public List<SelectItem> getArguments() {
		List<SelectItem> numberList = new ArrayList<SelectItem>();
		for (int i = 0; i < N_MAX; i++)
			numberList.add(new SelectItem(i, String.valueOf(i)));
		return numberList;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getFactorSum() {
		return factorSum;
	}
}
