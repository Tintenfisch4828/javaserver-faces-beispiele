package de.christopherolbertz.valuechangeevents.view;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.model.SelectItem;

/**
 * Diese Klasse fuellt die SelectOne-Komponenten mit Werten. 
 * @author olbertz
 *
 */
@ManagedBean
public class PreRenderViewBean {
	// ****************************** Attribute **************************************
	/**
	 * Eine Liste mit Laendern. Dabei muss es sich um Objekte der Klasse SelectItem handeln, damit die Listen von JSF etwas mit den 
	 * Listenelementen anfangen koennen. Die Laender werden in einer SelectOne-Liste angezeigt.
	 */
	private List<SelectItem> countryList;
	private List<Language> languageList;
	private String selectedGender;
	private String selectedLanguage;
	private String selectedCountry;
	
	public void initialize(ComponentSystemEvent event) {
		System.out.println("*********** ComponentSystemEvent ***********");
		countryList = new ArrayList<SelectItem>();
		countryList.add(new SelectItem("DE", "Deutschland"));
		countryList.add(new SelectItem("FR", "Frankreich"));
		countryList.add(new SelectItem("ES", "Spanien"));
		
		Language german = new Language("de", "Deusch");
		Language french = new Language("fr", "Französisch");
		Language spanish = new Language("es", "Spanisch");
		Language english = new Language("en", "Englisch");
		languageList = new ArrayList<Language>();
		languageList.add(german);
		languageList.add(french);
		languageList.add(spanish);
		languageList.add(english);
	}

	// ****************************** Getter und Setter ******************************
	public List<SelectItem> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<SelectItem> countryList) {
		this.countryList = countryList;
	}

	public List<Language> getLanguageList() {
		return languageList;
	}

	public void setLanguageList(List<Language> languageList) {
		this.languageList = languageList;
	}

	public String getSelectedGender() {
		return selectedGender;
	}

	public void setSelectedGender(String selectedGender) {
		this.selectedGender = selectedGender;
	}

	public String getSelectedLanguage() {
		return selectedLanguage;
	}

	public void setSelectedLanguage(String selectedLanguage) {
		this.selectedLanguage = selectedLanguage;
	}

	public String getSelectedCountry() {
		return selectedCountry;
	}

	public void setSelectedCountry(String selectedCountry) {
		this.selectedCountry = selectedCountry;
	}	
}
