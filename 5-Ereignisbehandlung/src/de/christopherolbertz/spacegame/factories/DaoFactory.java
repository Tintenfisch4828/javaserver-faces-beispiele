package de.christopherolbertz.spacegame.factories;

import de.christopherolbertz.spacegame.dao.classes.PlanetDaoImpl;
import de.christopherolbertz.spacegame.dao.classes.StarshipDaoImpl;
import de.christopherolbertz.spacegame.dao.classes.UserDaoImpl;
import de.christopherolbertz.spacegame.dao.interfaces.PlanetDao;
import de.christopherolbertz.spacegame.dao.interfaces.StarshipDao;
import de.christopherolbertz.spacegame.dao.interfaces.UserDao;

public class DaoFactory {
	public static StarshipDao getStarshipDao() {
		return StarshipDaoImpl.getInstance();
	}
	
	public static PlanetDao getPlanetDao() {
		return PlanetDaoImpl.getInstance();
	}
	
	public static UserDao getUserDao() {
		return UserDaoImpl.getInstance();
	}
}
