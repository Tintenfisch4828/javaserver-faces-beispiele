package de.christopherolbertz.spacegame.view.managedBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import de.christopherolbertz.spacegame.factories.ServiceFactory;
import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.model.interfaces.Starship;
import de.christopherolbertz.spacegame.model.interfaces.User;
import de.christopherolbertz.spacegame.service.interfaces.PlanetService;
import de.christopherolbertz.spacegame.service.interfaces.StarshipService;
import de.christopherolbertz.spacegame.service.interfaces.UserService;
import de.christopherolbertz.spacegame.utils.JsfUtils;

@ManagedBean
@ViewScoped
public class AttackAPlanetView implements Serializable {
	private static final long serialVersionUID = -749195364844497033L;
	
	private User attackingUser;
	private PlanetService planetService;
	private StarshipService starshipService;
	private List<Starship> availableStarships;
	private List<Long> selectedStarshipIds; 
	private boolean isPlanetDestroyed;
	private String selectedUserId;
	private List<User> users;
	private String selectedTargetPlanetId;
	private List<Planet> planetsOfEnemy;
	private List<Planet> planetsOfUser;
	private UserService userService;
	private long selectedStartPlanetId;
	private Planet startPlanet;
	
	@PostConstruct
	public void initialize() {
		planetService = ServiceFactory.createPlanetService();
		starshipService = ServiceFactory.createStarshipService();
		userService = ServiceFactory.createUserService();
		users = userService.findAllUsers();
		final long attackingUserId = JsfUtils.getUserIdParameter();
		attackingUser = userService.findUser(attackingUserId);
		planetsOfUser = attackingUser.getMyPlanets();
	}
	
	public void onSelectUserClicked(ActionEvent actionEvent) {
		final long userId = Long.parseLong(selectedUserId);
		final User selectedUser = userService.findUser(userId);
		planetsOfEnemy = selectedUser.getMyPlanets();
	}

	public void attack(ActionEvent event) {
		final List<Starship> myStarships = starshipService.findStarshipsByIds(selectedStarshipIds);
		final long targetPlanetId = Long.valueOf(selectedTargetPlanetId);
		Planet targetPlanet = planetService.findPlanet(targetPlanetId);
		isPlanetDestroyed = planetService.attackPlanet(targetPlanet, myStarships);
		
		//starshipService.attackPlanet(attackPosition, starships);
		//isPlanetDestroyed = planetService.isPlanetDestroyed(attackPosition);
		//destroyedStarships = starshipService.getDestroyedStarshipsFromList(starships);
	}
	
	public void onSelectStartPlanetClicked(ActionEvent actionEvent) {
		startPlanet = planetService.findPlanet
				(selectedStartPlanetId);
		availableStarships = starshipService.
				findAllStarshipsOnPlanet(startPlanet);
	}

	public List<Long> getSelectedStarshipIds() {
		return selectedStarshipIds;
	}

	public void setSelectedStarshipIds(List<Long> selectedStarshipIds) {
		this.selectedStarshipIds = selectedStarshipIds;
	}

	public List<Starship> getDestroyedStarships() {
		final List<Starship> destroyedStarships = new ArrayList<>();
		
		if (availableStarships != null) {
			for (final Starship starship: availableStarships) {
				if (starship.isDestroyed()) {
					destroyedStarships.add(starship);
				}
			}
		}
		
		return destroyedStarships;
	}

	public boolean isPlanetDestroyed() {
		return isPlanetDestroyed;
	}

	public void setPlanetDestroyed(boolean isPlanetDestroyed) {
		this.isPlanetDestroyed = isPlanetDestroyed;
	}

	public void setPlanetService(PlanetService planetService) {
		this.planetService = planetService;
	}

	public void setStarshipService(StarshipService starshipService) {
		this.starshipService = starshipService;
	}

	public List<Starship> getAvailableStarships() {
		return availableStarships;
	}

	public void setAvailableStarships(List<Starship> availableStarships) {
		this.availableStarships = availableStarships;
	}

	public String getSelectedUserId() {
		return selectedUserId;
	}

	public void setSelectedUserId(String selectedUserId) {
		this.selectedUserId = selectedUserId;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public String getSelectedTargetPlanetId() {
		return selectedTargetPlanetId;
	}

	public void setSelectedTargetPlanetId(String selectedTargetPlanetId) {
		this.selectedTargetPlanetId = selectedTargetPlanetId;
	}

	public List<Planet> getPlanetsOfUser() {
		return planetsOfUser;
	}

	public void setPlanetsOfUser(List<Planet> planetsOfUser) {
		this.planetsOfUser = planetsOfUser;
	}

	public List<Planet> getPlanetsOfEnemy() {
		return planetsOfEnemy;
	}

	public void setPlanetsOfEnemy(List<Planet> planetsOfEnemy) {
		this.planetsOfEnemy = planetsOfEnemy;
	}
	
	public long getSelectedStartPlanetId() {
		return selectedStartPlanetId;
	}
	
	public void setSelectedStartPlanetId(long selectedStartPlanetId) {
		this.selectedStartPlanetId = selectedStartPlanetId;
	}
}