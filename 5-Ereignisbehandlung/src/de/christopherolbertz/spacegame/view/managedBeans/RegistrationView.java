package de.christopherolbertz.spacegame.view.managedBeans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import de.christopherolbertz.spacegame.factories.ModelFactory;
import de.christopherolbertz.spacegame.factories.ServiceFactory;
import de.christopherolbertz.spacegame.model.interfaces.User;
import de.christopherolbertz.spacegame.service.interfaces.UserService;

@ManagedBean
@ViewScoped
public class RegistrationView implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5869735517745650160L;
	private String emailaddress;
	private String username;
	private String password;
	private String confirmedPassword;
	private int age;
	private UserService userService;

	public void onRegistrateNewUser(ActionEvent actionEvent) {
		final User user = ModelFactory.createUser
				(username, emailaddress, password);
		userService.saveUser(user);
	}
	
	@PostConstruct
	public void initialize() {
		userService = ServiceFactory.createUserService();
	}
	
	public String getEmailaddress() {
		return emailaddress;
	}
	
	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getConfirmedPassword() {
		return confirmedPassword;
	}
	
	public void setConfirmedPassword(String confirmedPassword) {
		this.confirmedPassword = confirmedPassword;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
}
