package de.christopherolbertz.spacegame.dao.classes;

import java.util.ArrayList;
import java.util.List;

import de.christopherolbertz.spacegame.dao.interfaces.PlanetDao;
import de.christopherolbertz.spacegame.model.classes.PlanetImpl;
import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.model.interfaces.User;
import de.christopherolbertz.spacegame.utils.RandomUtils;

public class PlanetDaoImpl implements PlanetDao {
	private static PlanetDao planetDao;
	private static List<Planet> planetTable;
	
	private PlanetDaoImpl() {
		planetTable = new ArrayList<Planet>();
		planetTable.add(new PlanetImpl(1, "Erde", 200, 300));
		planetTable.add(new PlanetImpl(2, "Vulkan", 200, 300));
		planetTable.add(new PlanetImpl(3, "Venus", 200, 300));
		planetTable.add(new PlanetImpl(4, "Centauri Prime", 200, 300));
		planetTable.add(new PlanetImpl(5, "Cronos", 200, 300));
		planetTable.add(new PlanetImpl(6, "Mars", 200, 300));
	}
	
	/*	private PlanetDaoImpl() {
		userDao = UserDaoImpl.getInstance();
		planetTable = new ArrayList<Planet>();

		final User picard = userDao.get(0);
		final User sisko = userDao.get(1);
		final User han = userDao.get(2);
		final User vader = userDao.get(3);
		
		final Planet erde = new PlanetImpl(1, "Erde", 200, 300);
		erde.setOwner(picard);
		
		planetTable.add(erde);
		
		final Planet mars = new PlanetImpl(2, "Mars", 200, 300);
		mars.setOwner(vader);
		
		planetTable.add(mars);
		final Planet venus = new PlanetImpl(3, "Venus", 200, 300);
		venus.setOwner(han);
		
		planetTable.add(venus);
		
		final Planet centauri = new PlanetImpl(4, "Centauri Prime", 200, 300);
		centauri.setOwner(sisko);
		planetTable.add(centauri);
		
		final Planet cronos = new PlanetImpl(5, "Cronos", 200, 300); 
		cronos.setOwner(picard);
		planetTable.add(cronos);
		
		final Planet vulkan = new PlanetImpl(6, "Vulkan", 200, 300);
		vulkan.setOwner(picard);
		planetTable.add(vulkan);
	}
	*/
	
	public static PlanetDao getInstance() {
		if (planetDao == null) {
			planetDao = new PlanetDaoImpl();
		}
		
		return planetDao;
	}
	
	@Override
	public void delete(final Planet planet){
		planetTable.remove(planet);
	}

	@Override
	public Planet findPlanet(final long planetId) {
		for(final Planet planet: planetTable) {
			if (planet.getPlanetId() == planetId) {
				return planet;
			}
		}
		
		return null;
	}
	
	@Override
	public List<Planet> findAllPlanets() {
		return planetTable;
	}
	
	@Override
	public void savePlanet(final Planet planet) {
		if (planet.getPlanetId() == 0) {
			final int nextId = planetTable.size() + 1;
			planet.setPlanetId(nextId);
		}
		planetTable.add(planet);
	}

	@Override
	public List<Planet> getList() {
		return planetTable;
	}
	
	@Override
	public List<Planet> findPlanetsByUser(final User user) {
		/*final List<Planet> planetList = new ArrayList<Planet>();
		
		for (final Planet planet: planetTable) {
			if (planet.getOwner().equals(user)) {
				planetList.add(planet);
			}
		}
		
		return planetList;*/
		return user.getMyPlanets();
	}
}
