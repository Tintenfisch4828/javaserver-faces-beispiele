package de.christopherolbertz.spacegame.dao.interfaces;

import java.util.List;

import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.model.interfaces.User;

public interface PlanetDao {
	public void delete(Planet planet);
	public Planet findPlanet(long id);
	public List<Planet> getList();
	//public List<Planet> getList(String searchString);
	//public void savePlanet(Planet planet);
	//public List<Planet> findPlanetsWithUndamagedStarships();
	//public List<Planet> findPlanetsWithShipyard();
	//public List<Planet> getList(User user);
	//public int countStarshipsOnPlanet(Planet planet);
	void savePlanet(Planet planet);
	List<Planet> findAllPlanets();
	List<Planet> findPlanetsByUser(User user);
}
