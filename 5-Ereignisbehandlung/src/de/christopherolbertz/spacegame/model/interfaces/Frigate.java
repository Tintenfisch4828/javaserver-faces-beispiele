package de.christopherolbertz.spacegame.model.interfaces;

public interface Frigate extends Starship {
	public int getCanonCount();
	public void setCanonCount(int canonCount);
	public double getShields();
	public void setShields(double shields);
	double getAttackStrength();
	double getDefendStrength();
}
