package de.christopherolbertz.spacegame.model.classes;

import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.model.interfaces.TransportShip;

public class TransportShipImpl extends StarshipImpl implements TransportShip {
	private int usedSpace;
	private int maxStockroom;
	
	public TransportShipImpl(String name, Planet currentPlanet, int usedSpace, int maxStockroom) {
		super(name, currentPlanet);
		this.usedSpace = usedSpace;
		this.maxStockroom = maxStockroom;
	}
	
	public TransportShipImpl(final long starshipId, final String name, final Planet currentPlanet, 
			final int usedSpace, final int maxStockroom) {
		super(starshipId, name, currentPlanet);
		this.usedSpace = usedSpace;
		this.maxStockroom = maxStockroom;
	}

	public int getUsedSpace() {
		return usedSpace;
	}

	public void setUsedSpace(int usedSpace) {
		this.usedSpace = usedSpace;
	}

	public int getMaxStockroom() {
		return maxStockroom;
	}

	public void setMaxStockroom(int maxStockroom) {
		this.maxStockroom = maxStockroom;
	}
	
	@Override
	public int getCrystalCost() {
		return 30;
	}

	@Override
	public int getMetalCost() {
		return 80;
	}
}
