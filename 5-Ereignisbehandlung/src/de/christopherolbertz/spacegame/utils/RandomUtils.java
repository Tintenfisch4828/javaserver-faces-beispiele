package de.christopherolbertz.spacegame.utils;

import java.util.Random;

public class RandomUtils {
	private static final Random random;
	
	
	static {
		random = new Random();
	}
	
	/**
	 * Erzeugt eine int-Zufallszahl von 0 bis zu einem Maximalwert.
	 * @param max Der Maximalwert.
	 * @return Die erzeugte Zufallszahl.
	 */
	public static int nextInt(int max) {
		return random.nextInt(max);
	}
	
	public static long createRandomStarshipId() {
		return nextLong();
	}
	
	public static long createRandomPlanetId() {
		return nextLong();
	}
	
	public static String createRandomPlanetName() {
		final long randomLong = nextLong();
		return String.valueOf(randomLong);
	}
	
	public static long nextLong() {
		return random.nextLong();
	}
}
