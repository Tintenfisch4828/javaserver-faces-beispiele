package de.christopherolbertz.actionevents.view;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

@ManagedBean
@ViewScoped
public class SimpleActionEvents {
	private int counter;

	public void increment(ActionEvent actionEvent) {
		counter++;
	}
	
	public void decrement(ActionEvent actionEvent) {
		counter--;
	}
	
	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}
}
