package de.christopherolbertz.dataevents.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

import de.christopherolbertz.spacegame.factories.ServiceFactory;
import de.christopherolbertz.spacegame.model.interfaces.Starship;
import de.christopherolbertz.spacegame.service.classes.PlanetServiceImpl;
import de.christopherolbertz.spacegame.service.interfaces.PlanetService;
import de.christopherolbertz.spacegame.service.interfaces.StarshipService;

@ManagedBean
@ViewScoped
public class StarshipTableView {
	private DataModel<Starship> starships;
	private Starship selectedStarship;
	private int selectedRowIndex;
	
	@PostConstruct
	public void initialize() {
		final StarshipService starshipService = ServiceFactory.createStarshipService();
		final List<Starship> starshipList = starshipService.findAllStarships();
		starships = new ListDataModel<>();
		starships.setWrappedData(starshipList);
	}
	
	public void showStarship(ActionEvent event) {
		selectedStarship = starships.getRowData();
		selectedRowIndex = starships.getRowIndex();
	}

	public DataModel<Starship> getStarships() {
		return starships;
	}

	public void setStarships(DataModel<Starship> starships) {
		this.starships = starships;
	}

	public Starship getSelectedStarship() {
		return selectedStarship;
	}

	public void setSelectedStarship(Starship selectedStarship) {
		this.selectedStarship = selectedStarship;
	}

	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}

	public void setSelectedRowIndex(int selectedRowIndex) {
		this.selectedRowIndex = selectedRowIndex;
	}
}
