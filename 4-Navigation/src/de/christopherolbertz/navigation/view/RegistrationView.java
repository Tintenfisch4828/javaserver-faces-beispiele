package de.christopherolbertz.navigation.view;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

@ManagedBean
@SessionScoped
public class RegistrationView implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5869735517745650160L;
	private String emailaddress;
	private String username;
	private String password;
	private String confirmedPassword;
	private int age;
	
	public String registrate() {
		if (!password.equals(confirmedPassword)) {
			 return "passwordsNotEqual";
		} else if (validateEmailaddress() == false) {
			return "emailaddressNotValid";
		} else {
			return "registrationSuccessful";
		}
	}
	
	private boolean validateEmailaddress() {
		if (emailaddress.contains("@") && emailaddress.contains(".")) {
			return true;
		} else {
			return false;
		}
	}
	
	public String getEmailaddress() {
		return emailaddress;
	}
	
	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getConfirmedPassword() {
		return confirmedPassword;
	}
	
	public void setConfirmedPassword(String confirmedPassword) {
		this.confirmedPassword = confirmedPassword;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
}
