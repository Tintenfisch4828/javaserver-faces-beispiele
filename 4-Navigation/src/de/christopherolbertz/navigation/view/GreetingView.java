package de.christopherolbertz.navigation.view;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;

import de.christopherolbertz.navigation.utils.JsfUtil;

@ManagedBean
@RequestScoped
public class GreetingView {
	private String userName;
	
	@PostConstruct
	public void initialize() {
		userName = JsfUtil.getUsernameFromFlash();
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
}
