package de.christopherolbertz.navigation.utils;

import javax.faces.context.FacesContext;
import javax.faces.context.Flash;

public class JsfUtil {
	public static Flash getFlash() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		return facesContext.getExternalContext().getFlash();
	}
	
	public static String getUsernameFromFlash() {
		return (String)getFlash().get("username");
	}
}
