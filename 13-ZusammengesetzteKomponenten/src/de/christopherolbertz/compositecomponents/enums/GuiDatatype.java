package de.christopherolbertz.compositecomponents.enums;

public enum GuiDatatype {
	BOOLEAN("boolean"), DATE("date"), INT("int"), 
	STRING("string"), TIME("time");
	
	private String datatypeName;
	
	private GuiDatatype(String datatypeName) {
		this.datatypeName = datatypeName;
	}
	
	public static GuiDatatype findDatatypeByName(
			String datatypeName) {
		for (GuiDatatype datatype: values()) {
			if (datatype.toString().equals(datatypeName))
				return datatype;
		}
		return null;
	}
	
	public String toString() {
		return datatypeName;
	}
	
	public boolean isBoolean() {
		if (datatypeName.equals(BOOLEAN.toString())) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isDate() {
		if (datatypeName.equals(DATE.toString())) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isInt() {
		if (datatypeName.equals(INT.toString())) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isString() {
		if (datatypeName.equals(STRING.toString())) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isTime() {
		if (datatypeName.equals(TIME.toString())) {
			return true;
		} else {
			return false;
		}
	}
}
