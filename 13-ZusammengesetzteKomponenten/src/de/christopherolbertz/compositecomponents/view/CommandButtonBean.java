package de.christopherolbertz.compositecomponents.view;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;

@ManagedBean
@SessionScoped
public class CommandButtonBean implements Serializable {
	 
	private static final long serialVersionUID = -6989441633260622062L;
	private int count;
	 
	public int getCount() {
		return count;
	}
	 
	public void update(AjaxBehaviorEvent event) {
		count++;
	}
}