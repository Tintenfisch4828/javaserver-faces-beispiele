package de.christopherolbertz.compositecomponents.view;

/**
 * Enthaelt Informationen ueber Programmiersprachen.
 * @author olbertz
 *
 */
public class ProgrammingLanguage {
	/**
	 * Eine Identifizierungsnummer fuer die einzelnen Sprachen.
	 */
	private int id;
	/**
	 * Der Name der Programmiersprache.
	 */
	private String programmingLanguage;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public ProgrammingLanguage(int id, String programmingLanguage) {
		super();
		this.id = id;
		this.programmingLanguage = programmingLanguage;
	}

	public String getProgrammingLanguage() {
		return programmingLanguage;
	}
	
	public void setProgrammingLanguage(String programmingLanguage) {
		this.programmingLanguage = programmingLanguage;
	}
	
	@Override
	public String toString() {
		return id + ": " + programmingLanguage;
	}
}
