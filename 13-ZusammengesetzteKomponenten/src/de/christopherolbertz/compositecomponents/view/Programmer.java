package de.christopherolbertz.compositecomponents.view;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

/**
 * Repraesentiert einen Programmierer.
 * @author christopher
 *
 */
@ManagedBean
public class Programmer {
	private String firstName;
	private String lastName;
	private List<ProgrammingLanguage> languages;
	private String level;
	
	public Programmer() {
		firstName = "Christopher";
		lastName = "Olbertz";
		languages = new ArrayList<ProgrammingLanguage>();
		languages.add(new ProgrammingLanguage(1, "Java"));
		languages.add(new ProgrammingLanguage(2, "C#"));
		languages.add(new ProgrammingLanguage(3, "VBA"));
		languages.add(new ProgrammingLanguage(4, "C++"));
		level = "Oberguru";
	}
	
	public Programmer(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	@Override
	public String toString() {
		return firstName + " " + lastName;
	}

	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public List<ProgrammingLanguage> getLanguages() {
		return languages;
	}
	
	public void setLanguages(List<ProgrammingLanguage> languages) {
		this.languages = languages;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
}
