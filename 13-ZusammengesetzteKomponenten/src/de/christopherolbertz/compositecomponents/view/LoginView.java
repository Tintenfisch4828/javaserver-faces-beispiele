package de.christopherolbertz.compositecomponents.view;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

//@ManagedBean
//@SessionScoped
public class LoginView {
	private String username;
	private String password;
	
	public String login() {
		System.out.println("********* Eingeloggt!!! *********");
		System.out.println("********* Benutzername: " + username + "*********");
		System.out.println("********* Passwort: " + password + "*********");
		return "login";
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
}