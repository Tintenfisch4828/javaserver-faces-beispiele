package de.christopherolbertz.spacegame.view;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import de.christopherolbertz.spacegame.model.interfaces.Starship;
import de.christopherolbertz.spacegame.service.classes.StarshipServiceImpl;
import de.christopherolbertz.spacegame.service.interfaces.StarshipService;

@ManagedBean
@ViewScoped
public class RenameStarshipView {
	private String starshipName;
	private String oldStarshipName;
	private Starship starship;
	private StarshipService starshipService;

	@PostConstruct
	public void init() {
		starshipService = new StarshipServiceImpl();
		FacesContext facesContext = FacesContext.getCurrentInstance();
		oldStarshipName = facesContext.getExternalContext().
				getRequestParameterMap().get("oldStarshipName");
		int starshipId = Integer.parseInt(facesContext.getExternalContext().
				getRequestParameterMap().get("starshipId"));
		starship = starshipService.findStarship(starshipId);
	}
	
	public void renameStarship(ActionEvent event) {
		starship.setStarshipName(starshipName);
		starshipService.saveStarship(starship);
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.addMessage(null, 
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Aenderung", 
						"Name des Raumschiffs wurde erfolgreich geändert zu " + 
							starshipName));
	}
	
	public String getStarshipName() {
		return starshipName;
	}

	public void setStarshipName(String starshipName) {
		this.starshipName = starshipName;
	}

	public String getOldStarshipName() {
		return oldStarshipName;
	}

	public void setOldStarshipName(String oldStarshipName) {
		this.oldStarshipName = oldStarshipName;
	}

	public Starship getStarship() {
		return starship;
	}

	public void setStarship(Starship starship) {
		this.starship = starship;
	}
}
