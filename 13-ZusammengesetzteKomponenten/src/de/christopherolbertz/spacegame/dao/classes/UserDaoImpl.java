package de.christopherolbertz.spacegame.dao.classes;

import java.util.ArrayList;
import java.util.List;

import de.christopherolbertz.spacegame.dao.interfaces.PlanetDao;
import de.christopherolbertz.spacegame.dao.interfaces.UserDao;
import de.christopherolbertz.spacegame.model.classes.UserImpl;
import de.christopherolbertz.spacegame.model.interfaces.User;
import de.christopherolbertz.spacegame.utils.StringUtils;

public class UserDaoImpl implements UserDao {
	private List<User> userTable;
	private static UserDao userDao;
	private PlanetDao planetDao;
	
	private UserDaoImpl() {
		userTable = new ArrayList<>();
		planetDao = PlanetDaoImpl.getInstance();
		userTable.add(new UserImpl(1, "jeanluc", "picard@starfleet.gov", "geheim"));
		userTable.get(0).getMyPlanets().add(planetDao.findPlanet(1));
		userTable.get(0).getMyPlanets().add(planetDao.findPlanet(2));
		userTable.get(0).getMyPlanets().add(planetDao.findPlanet(3));
		userTable.add(new UserImpl(2, "sisko", "sisko@starfleet.gov", "sehrgeheim"));
		userTable.get(1).getMyPlanets().add(planetDao.findPlanet(4));
		userTable.get(1).getMyPlanets().add(planetDao.findPlanet(5));
		userTable.get(1).getMyPlanets().add(planetDao.findPlanet(6));
		userTable.add(new UserImpl(3, "hanSolo", "solo@rebellenmail.de", "nochgeheimer"));
		userTable.add(new UserImpl(4, "vader", "darthvader@imperium.com", "supergeheim"));
	}
	
	public static UserDao getInstance() {
		if (userDao == null) {
			userDao = new UserDaoImpl();
		}
		
		return userDao;
	}
	
	@Override
	public void delete(final User user) {
		userTable.remove(user);		
	}

	@Override
	public User findUser(long userId) {
		for (final User user: userTable) {
			if (user.getUserId() == userId) {
				return user;
			}
		}
		
		return null;
	}

	@Override
	public User findUserByName(final String userName) {
		for (final User user: userTable) {
			if (StringUtils.areStringsEqual(user.getUsername(), userName)) {
				return user;
			}
		}
		
		return null;
	}
	
	@Override
	public List<User> getList() {
		return userTable;
	}

	@Override
	public void saveUser(User user) {
		userTable.add(user);
	}
	
	@Override
	public int countUsers() {
		return userTable.size();
	}
}
