package de.christopherolbertz.spacegame.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import de.christopherolbertz.spacegame.factories.ServiceFactory;
import de.christopherolbertz.spacegame.model.interfaces.User;
import de.christopherolbertz.spacegame.service.interfaces.UserService;

@FacesConverter(forClass = User.class)
public class UserConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		final UserService userService = ServiceFactory.createUserService();
		final User user = userService.findUserByName(value);
		return user;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		return value.toString();
	}

}
