package de.christopherolbertz.spacegame.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import de.christopherolbertz.spacegame.i18n.I18nMessageUtil;

@FacesValidator("htwEmailaddressValidator")
public class HTWEmailaddressValidator implements Validator {

	public void validate(FacesContext context, 
											UIComponent component, 
											Object value) throws ValidatorException {
		String emailaddress = (String)value;
		
		if (!emailaddress.endsWith("htwsaar.de")) {
			throw new ValidatorException(new FacesMessage(
					I18nMessageUtil.getNotAHtwEmailString()));
		}
	}

}
