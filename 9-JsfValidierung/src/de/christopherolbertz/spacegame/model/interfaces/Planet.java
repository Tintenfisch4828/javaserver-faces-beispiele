package de.christopherolbertz.spacegame.model.interfaces;

import java.util.List;

public interface Planet {
	public abstract long getPlanetId();
	public abstract void setPlanetId(long planetId);
	public abstract String getPlanetName();
	public abstract void setPlanetName(String planetName);
	public abstract int getMetalStorage();
	public abstract void setMetalStorage(int metalStorage);
	public abstract int getCrystalStorage();
	public abstract void setCrystalStorage(int crystalStorage);
	public abstract List<Starship> getStarshipsOnPlanet();
	public abstract void setStarshipsOnPlanet(
			List<Starship> starshipsOnPlanet);
	public abstract int hashCode();
	public abstract boolean equals(Object obj);
	public abstract String toString();
	public void landStarship(Starship starship);
	public void landStarshipFleet(List<Starship> starships);
	public int getMaxStarshipCapacity();
	public void setMaxStarshipCapacity(int maxStarshipCapacity);
	void reduceMetalStorage(int metalAmount);
	void reduceCrystalStorage(int crystalAmount);
	void setOwner(User owner);
	User getOwner();
}