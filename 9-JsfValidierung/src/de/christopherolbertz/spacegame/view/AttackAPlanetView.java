package de.christopherolbertz.spacegame.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import de.christopherolbertz.spacegame.factories.ServiceFactory;
import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.model.interfaces.Starship;
import de.christopherolbertz.spacegame.model.interfaces.User;
import de.christopherolbertz.spacegame.service.interfaces.PlanetService;
import de.christopherolbertz.spacegame.service.interfaces.StarshipService;
import de.christopherolbertz.spacegame.service.interfaces.UserService;
import de.christopherolbertz.spacegame.utils.JsfUtils;

@ManagedBean
@ViewScoped
public class AttackAPlanetView implements Serializable {
	private static final long serialVersionUID = -749195364844497033L;
	
	private User attackingUser;
	private PlanetService planetService;
	private StarshipService starshipService;
	private List<Starship> availableStarships;
	private List<Starship> selectedStarships; 
	private boolean isPlanetDestroyed;
	private User selectedUser;
	private List<User> users;
	private String selectedTargetPlanetId;
	private List<Planet> planetsOfEnemy;
	private List<Planet> planetsOfUser;
	private UserService userService;
	private Planet selectedStartPlanet;
	private Planet startPlanet;
	
	@PostConstruct
	public void initialize() {
		planetService = ServiceFactory.createPlanetService();
		starshipService = ServiceFactory.createStarshipService();
		userService = ServiceFactory.createUserService();
		users = userService.findAllUsers();
		final long attackingUserId = JsfUtils.getUserIdParameter();
		attackingUser = userService.findUser(attackingUserId);
		planetsOfUser = attackingUser.getMyPlanets();
	}
	
	public void onSelectUserClicked(ActionEvent actionEvent) {
		planetsOfEnemy = selectedUser.getMyPlanets();
	}

	public void attack(ActionEvent event) {
		final long targetPlanetId =  Long.valueOf(selectedTargetPlanetId);
		Planet targetPlanet = planetService.findPlanet(targetPlanetId);
		isPlanetDestroyed = planetService.attackPlanet(targetPlanet, selectedStarships);
	}
	
	public void onSelectStartPlanetClicked(ActionEvent actionEvent) {
		availableStarships = starshipService.
				findAllStarshipsOnPlanet(selectedStartPlanet);
	}

	public List<Starship> getDestroyedStarships() {
		final List<Starship> destroyedStarships = new ArrayList<>();
		
		if (availableStarships != null) {
			for (final Starship starship: availableStarships) {
				if (starship.isDestroyed()) {
					destroyedStarships.add(starship);
				}
			}
		}
		
		return destroyedStarships;
	}

	public boolean isPlanetDestroyed() {
		return isPlanetDestroyed;
	}

	public void setPlanetDestroyed(boolean isPlanetDestroyed) {
		this.isPlanetDestroyed = isPlanetDestroyed;
	}

	public void setPlanetService(PlanetService planetService) {
		this.planetService = planetService;
	}

	public void setStarshipService(StarshipService starshipService) {
		this.starshipService = starshipService;
	}

	public List<Starship> getAvailableStarships() {
		return availableStarships;
	}

	public void setAvailableStarships(List<Starship> availableStarships) {
		this.availableStarships = availableStarships;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public String getSelectedTargetPlanetId() {
		return selectedTargetPlanetId;
	}

	public void setSelectedTargetPlanetId(String selectedTargetPlanetId) {
		this.selectedTargetPlanetId = selectedTargetPlanetId;
	}

	public List<Planet> getPlanetsOfUser() {
		return planetsOfUser;
	}

	public void setPlanetsOfUser(List<Planet> planetsOfUser) {
		this.planetsOfUser = planetsOfUser;
	}

	public List<Planet> getPlanetsOfEnemy() {
		return planetsOfEnemy;
	}

	public void setPlanetsOfEnemy(List<Planet> planetsOfEnemy) {
		this.planetsOfEnemy = planetsOfEnemy;
	}

	public List<Starship> getSelectedStarships() {
		return selectedStarships;
	}

	public void setSelectedStarships(List<Starship> selectedStarships) {
		this.selectedStarships = selectedStarships;
	}

	public User getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}

	public Planet getSelectedStartPlanet() {
		return selectedStartPlanet;
	}

	public void setSelectedStartPlanet(Planet selectedStartPlanet) {
		this.selectedStartPlanet = selectedStartPlanet;
	}
	
}