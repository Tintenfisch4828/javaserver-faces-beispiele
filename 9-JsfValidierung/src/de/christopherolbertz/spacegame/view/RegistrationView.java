package de.christopherolbertz.spacegame.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;

import de.christopherolbertz.spacegame.factories.ModelFactory;
import de.christopherolbertz.spacegame.factories.ServiceFactory;
import de.christopherolbertz.spacegame.model.classes.Language;
import de.christopherolbertz.spacegame.model.interfaces.User;
import de.christopherolbertz.spacegame.service.interfaces.UserService;

@ManagedBean
@ViewScoped
public class RegistrationView implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5869735517745650160L;
	private String emailaddress;
	private String username;
	private String password;
	private String confirmedPassword;
	private int age;
	private UserService userService;
	private List<Language> languageList;
	private Language selectedLanguage;
	private Locale selectedLocale;

	public void onRegistrateNewUser(ActionEvent actionEvent) {
		final User user = ModelFactory.createUser
				(username, emailaddress, password);
		userService.saveUser(user);
	}
	
	@PostConstruct
	public void initialize() {
		userService = ServiceFactory.createUserService(); 
	}
	
	public void validateEmailaddress(FacesContext facesContext, UIComponent component,
			Object value) throws ValidatorException {
		String email = (String)value;
		
		if (!(email.contains("@") && email.contains("."))) {
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Fehler!", "Die Emailadresse ist nicht korrekt!"));
		}
	}
	
	public String getEmailaddress() {
		return emailaddress;
	}
	
	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getConfirmedPassword() {
		return confirmedPassword;
	}
	
	public void setConfirmedPassword(String confirmedPassword) {
		this.confirmedPassword = confirmedPassword;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}

	public List<Language> getLanguageList() {
		return languageList;
	}

	public void setLanguageList(List<Language> languageList) {
		this.languageList = languageList;
	}

	public Language getSelectedLanguage() {
		return selectedLanguage;
	}

	public void setSelectedLanguage(Language selectedLanguage) {
		this.selectedLanguage = selectedLanguage;
	}

	public Locale getSelectedLocale() {
		return selectedLocale;
	}

	public void setSelectedLocale(Locale selectedLocale) {
		this.selectedLocale = selectedLocale;
	}
}
