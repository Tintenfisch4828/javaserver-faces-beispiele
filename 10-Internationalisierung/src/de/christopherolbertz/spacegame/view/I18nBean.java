package de.christopherolbertz.spacegame.view;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;

import de.christopherolbertz.spacegame.model.classes.Language;
import de.christopherolbertz.spacegame.utils.JsfUtils;
import de.christopherolbertz.spacegame.utils.StringUtils;

/**
 * This bean manages the internationalization. It contains the available languages and the listeners that are fired
 * when the language is changed. 
 * @author Christopher Olbertz
 *
 */
@ManagedBean
@SessionScoped
public class I18nBean implements Serializable {
	private List<Language> languages;
	private String localeCode;
	
	@PostConstruct
	public void initialize() {
		languages = new ArrayList<>();
		languages.add(new Language(Locale.GERMAN, "Deutsch"));
		languages.add(new Language(Locale.ENGLISH, "Englisch"));
		localeCode = Locale.GERMAN.getLanguage();
	}
	
	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	public String getLocaleCode() {
		return localeCode;
	}

	public void setLocaleCode(String localeCode) {
		this.localeCode = localeCode;
	}
}