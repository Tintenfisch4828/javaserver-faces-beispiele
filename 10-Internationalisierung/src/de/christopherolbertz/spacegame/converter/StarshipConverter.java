package de.christopherolbertz.spacegame.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import de.christopherolbertz.spacegame.factories.ServiceFactory;
import de.christopherolbertz.spacegame.model.interfaces.Starship;
import de.christopherolbertz.spacegame.service.interfaces.StarshipService;

@FacesConverter("starshipConverter")
public class StarshipConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		final StarshipService starshipService = ServiceFactory.createStarshipService();
		final long starshipId = Long.parseLong(value);
		final Starship starship = starshipService.findStarship(starshipId);
		return starship;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		return value.toString();
	}

}
