package de.christopherolbertz.komponenten.model;

/**
 * Eine L�nderklasse, die als Attribute den ISO-Code des Landes und den Namen enth�lt. In einer Liste in der grafischen Oberfl�che dient
 * der ISO-Code als value und der L�ndername als label. In einer realen Applikation k�nnten diese Daten z.B. aus einer Datenbank 
 * gelesen werden.
 * 
 * @author christopher
 *
 */
public class Country {
	// ********************************************* Attribute ************************************
	/**
	 * Der ISO-Code des Landes.
	 */
	private String isoCode;
	/**
	 * Der Name des Landes.
	 */
	private String countryName;
	/**
	 * Die Anzahl der Einwohner des Landes.
	 */
	private int population;
	/**
	 * Der Name der Datei mit der Flagge des Landes.
	 */
	private String flagFileName;
	
	private boolean inEurope;
	
	// ********************************************* Konstruktoren ********************************
	/**
	 * Initialisiert das Objekt.
	 */
	public Country(String isoCode, String countryName, int population, String flagFileName) {
		this(isoCode, countryName, population, flagFileName, true);
	}
	
	public Country(String isoCode, String countryName, int population, String flagFileName, boolean inEurope) {
		this.isoCode = isoCode;
		this.countryName = countryName;
		this.population = population;
		this.flagFileName = flagFileName;
		this.inEurope = inEurope;
	}

	// ********************************************* Methoden *************************************
	@Override
	public String toString() {
		return isoCode + ": " + countryName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((isoCode == null) ? 0 : isoCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		
		// Wenn obj ein String ist, kann man davon ausgehen, dass es sich um einen Iso-Code handelt.
		if (obj instanceof String) {
			String isoCode = (String) obj;
			// Codes miteinander vergleichen.
			if (isoCode.equals(this.isoCode)) {
				return true;
			} else {
				return false;
			}
		}
		
		// Wenn es sich nicht um einen String handelt, wird normal mit equals() weitergemacht.
		if (getClass() != obj.getClass())
			return false;
		Country other = (Country) obj;
		if (isoCode == null) {
			if (other.isoCode != null)
				return false;
		} else if (!isoCode.equals(other.isoCode))
			return false;
		return true;
	}

	public String getIsoCode() {
		return isoCode;
	}
	
	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}
	
	public String getCountryName() {
		return countryName;
	}
	
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	
	public int getPopulation() {
		return population;
	}
	
	public void setPopulation(int population) {
		this.population = population;
	}
	
	public String getFlagFileName() {
		return flagFileName;
	}
	
	public void setFlagFileName(String flagFileName) {
		this.flagFileName = flagFileName;
	}
	
	public String getFlag() {
		return "flags/" + flagFileName + ".png";
	}
	
	public boolean isInEurope() {
		return inEurope;
	}
	
	public void setInEurope(boolean inEurope) {
		this.inEurope = inEurope;
	}
}

