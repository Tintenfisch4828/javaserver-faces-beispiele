package de.christopherolbertz.komponenten.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.model.SelectItem;

import de.christopherolbertz.komponenten.model.Language;

/**
 * Diese Klasse fuellt die SelectOne-Komponenten mit Werten. 
 * @author olbertz
 *
 */
@ManagedBean
@RequestScoped
public class DemoSelectOne {
	/**
	 * Eine Liste mit Laendern. Dabei muss es sich um Objekte der Klasse SelectItem handeln, damit die Listen von JSF etwas mit den 
	 * Listenelementen anfangen koennen. Die Laender werden in einer SelectOne-Liste angezeigt.
	 */
	private List<SelectItem> countryList;
	private List<Language> languageList;
	private String selectedGender;
	private String selectedLanguage;
	private String selectedCountry;
	
	@PostConstruct
	public void initialize() {
		System.out.println("++++++++++++++++++");
		countryList = new ArrayList<SelectItem>();
		countryList.add(new SelectItem("DE", "Deutschland"));
		countryList.add(new SelectItem("FR", "Frankreich"));
		countryList.add(new SelectItem("ES", "Spanien"));
		
		Language german = new Language("de", "Deutsch");
		Language french = new Language("fr", "Franzoesisch");
		Language spanish = new Language("es", "Spanisch");
		Language english = new Language("en", "Englisch");
		languageList = new ArrayList<Language>();
		languageList.add(german);
		languageList.add(french);
		languageList.add(spanish);
		languageList.add(english);
	}
	
	/*
	 Notwendig fuer die Language-Objekte vor JSF 2.0
	 
	private List<SelectItem> languageSelectItems;
	
	public DemoSelectOne() {
		for (Language language: languageList) {
			SelectItem selectItem = new SelectItem(
					language.getLanguageCode(), 
					language.getLanguageName());
			languageSelectItems.add(selectItem);
		}
	}*/

	// ****************************** Getter und Setter ******************************
	public List<SelectItem> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<SelectItem> countryList) {
		this.countryList = countryList;
	}

	public List<Language> getLanguageList() {
		return languageList;
	}

	public void setLanguageList(List<Language> languageList) {
		this.languageList = languageList;
	}

	public String getSelectedGender() {
		return selectedGender;
	}

	public void setSelectedGender(String selectedGender) {
		this.selectedGender = selectedGender;
	}

	public String getSelectedLanguage() {
		return selectedLanguage;
	}

	public void setSelectedLanguage(String selectedLanguage) {
		this.selectedLanguage = selectedLanguage;
	}

	public String getSelectedCountry() {
		return selectedCountry;
	}

	public void setSelectedCountry(String selectedCountry) {
		this.selectedCountry = selectedCountry;
	}	
}
