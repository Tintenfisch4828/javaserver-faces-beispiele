package de.christopherolbertz.komponenten.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.model.DataModel;
import javax.faces.model.DataModelEvent;
import javax.faces.model.DataModelListener;
import javax.faces.model.ListDataModel;

import de.christopherolbertz.komponenten.model.Country;
import de.christopherolbertz.komponenten.service.CountryService;

/**
 * Diese Managed Bean dient der Demonstration von Tabellen in JSF. Die Liste mit den Laendern wird dabei auf der JSF-Seite als 
 * Tabelle ausgegeben. 
 * @author christopher
 *
 */
@ManagedBean
@ViewScoped
public class DemoDataTable {
	private DataModel<Country> dataModel;
	private int selectedIndex;
	private CountryService countryService;
	
	@PostConstruct
	public void initialize() {
		countryService = CountryService.getInstance();
		dataModel = new ListDataModel<>();
		loadCountries();
	}
	
	public void deleteCountry(ActionEvent actionEvent) {
		selectedIndex = dataModel.getRowIndex();
		countryService.removeCountry(selectedIndex);
		loadCountries();
	}
	
	private void loadCountries() {
		List<Country> countryList = countryService.findAllCountries();
		dataModel.setWrappedData(countryList);
	}
	
	public DataModel<Country> getDataModel() {
		return dataModel;
	}
	
	public void setDataModel(DataModel<Country> dataModel) {
		this.dataModel = dataModel;
	}
	
	public int getSelectedIndex() {
		return selectedIndex;
	}
}
