package de.christopherolbertz.komponenten.view;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

@ManagedBean
public class DemoOutputLabel {
	private String firstName;
	private String lastName;
	
	@PostConstruct
	public void initialize() {
		firstName = "Anna";
		lastName = "Nass";
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}