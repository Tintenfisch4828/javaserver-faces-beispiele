package de.christopherolbertz.komponenten.view;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

@ManagedBean
@ViewScoped
public class DemoCommand {
	private String lastname;
	private String firstname;
	private boolean firstnameExists;
	private String outputTextDisabledText;
	
	@PostConstruct
	public void initialize() {
		outputTextDisabledText = "Klick die Schaltfläche, um mich zu aktivieren!";
	}
	
	public void onClick(ActionEvent actionEvent) {
		if (firstname != null && !firstname.isEmpty()) {
			firstnameExists = true;
			outputTextDisabledText = "Danke!";
		}
	}
	
	public String getLastname() {
		return lastname;
	}
	
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	public String getFirstname() {
		return firstname;
	}
	
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public boolean isFirstnameExists() {
		return firstnameExists;
	}

	public void setFirstnameExists(boolean firstnameExists) {
		this.firstnameExists = firstnameExists;
	}

	public String getOutputTextDisabledText() {
		return outputTextDisabledText;
	}

	public void setOutputTextDisabledText(String outputTextDisabledText) {
		this.outputTextDisabledText = outputTextDisabledText;
	}
}