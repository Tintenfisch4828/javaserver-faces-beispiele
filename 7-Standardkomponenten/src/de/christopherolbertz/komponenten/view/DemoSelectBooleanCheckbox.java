package de.christopherolbertz.komponenten.view;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class DemoSelectBooleanCheckbox {
	/**
	 * True, wenn der Benutzer den Newsletter abonnieren moechte, sonst false.
	 */
	private boolean newsletter;
	
	public boolean isNewsletter() {
		return newsletter;
	}

	public void setNewsletter(boolean newsletter) {
		this.newsletter = newsletter;
	}
}