package de.christopherolbertz.komponenten.view;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import de.christopherolbertz.komponenten.service.CountryService;

@ManagedBean
@ViewScoped
public class UpdateCountryView {
	private String countryName;
	private String countryCode;
	private int newPopulation;
	private CountryService countryService;
	
	@PostConstruct
	public void init() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		countryName = facesContext.getExternalContext().
				getRequestParameterMap().get("countryName");
		countryCode = facesContext.getExternalContext().
				getRequestParameterMap().get("countryCode");
		countryService = CountryService.getInstance();
		newPopulation = countryService.findPopulation(countryCode);
	}
	
	public void updatePopulation(ActionEvent actionEvent) {
		countryService.updatePopulation(countryCode, newPopulation);
	}
	
	public int getNewPopulation() {
		return newPopulation;
	}
	
	public void setNewPopulation(int newPopulation) {
		this.newPopulation = newPopulation;
	}
	
	public String getCountryName() {
		return countryName;
	}
}
