package de.christopherolbertz.komponenten.view;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

@ManagedBean
public class DemoOutputFormat {
	private String firstName;
	private String lastName;
	private String company;
	
	@PostConstruct
	public void initialize() {
		firstName = "Tom";
		lastName = "Ate";
		company = "Gemüse AG";
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCompany() {
		return company;
	}
	
	public void setCompany(String company) {
		this.company = company;
	}
}