package de.christopherolbertz.jsfkonvertierung.listeners;

import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;

public class MyActionListener implements ActionListener {
	private static int counter = 0;
	
	@Override
	public void processAction(ActionEvent actionEvent) throws AbortProcessingException {
		counter++;
		final UIComponent component = actionEvent.getComponent();
		final UIComponent parent = component.getParent();
		
		for (final UIComponent child: parent.getChildren()) {
			if (child instanceof HtmlOutputLabel) {
				final HtmlOutputLabel outputLabel = (HtmlOutputLabel)child;
				outputLabel.setValue("ActionEvent wurde zum " + counter + ". Mal ausgelöst!");
			}
		}
		
	}

}
