package de.christopherolbertz.jsfkonvertierung.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import de.christopherolbertz.jsfkonvertierung.model.Language;

@FacesConverter(forClass = Language.class)
public class LanguageConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, 
								UIComponent component, 
								String value) {
		System.out.println("getAsObject()");
		if (value.equals("de")) {
			return new Language("de", "Deusch");
		} else if (value.equals("fr")) {
			return new Language("fr", "Französisch");
		} else if (value.equals("en")) {
			return new Language("en", "Englisch");
		} else { 
			return new Language("es", "Spanisch");
		}
	}

	@Override
	public String getAsString(FacesContext context, 
							  UIComponent component, 
							  Object value) {
		System.out.println("getAsString()");
		Language language = (Language)value;
		return language.getLanguageName();
	}
}
