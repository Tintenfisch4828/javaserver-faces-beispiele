package de.christopherolbertz.jsfkonvertierung.converter;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import de.christopherolbertz.jsfkonvertierung.model.Pk;

//@FacesConverter("pkConverter")
public class PkConverter implements Converter<Pk> {
	private static final int DATE_LENGTH = 6;
	private static final int LENGTH = 14;
	private static final String WRONG_LENGTH = 
			"Eine gueltige PK hat genau 14 Stellen";
	private static final String INVALID_DATE = "Das Datum ist nicht gueltig!";
	
	@Override
	public Pk getAsObject(FacesContext facesContext, UIComponent component, 
			String value) {
		Pk pk = null;
		
		if (value.length() != LENGTH) {
			final FacesMessage facesMessage = new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					WRONG_LENGTH, WRONG_LENGTH);
			facesContext.addMessage(component.getClientId(), facesMessage);
		} else {
			final String[] pkElements = value.split("-");
			if (pkElements[0].length() != DATE_LENGTH) {
				final FacesMessage facesMessage = new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						INVALID_DATE, INVALID_DATE);
				facesContext.addMessage(component.getClientId(), facesMessage);
			} else {
				final String day = pkElements[0].substring(0, 2);
				final String month = pkElements[0].substring(2, 4);
				final String year = pkElements[0].substring(4, 6);
				final Calendar date = new GregorianCalendar(Integer.parseInt(year),
						Integer.parseInt(month), Integer.parseInt(day));
				final char firstLetter = pkElements[1].charAt(0);
				final String kwa = pkElements[2].substring(0, 3);
				final String number = pkElements[2].substring(3);
				pk = new Pk(date, firstLetter, Integer.parseInt(kwa), Integer.parseInt(number));
			}
		}
		
		return pk;
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent component, Pk value) {
		return value.toString();
	}
	
}
