package de.christopherolbertz.jsfkonvertierung.model;

public class Language {
	private String languageCode;
	private String languageName;
	
	public Language(String languageCode, String languageName) {
		super();
		this.languageCode = languageCode;
		this.languageName = languageName;
	}
	
	public String getLanguageCode() {
		return languageCode;
	}
	
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	
	public String getLanguageName() {
		return languageName;
	}
	
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((languageCode == null) ? 0 : languageCode.hashCode());
		result = prime * result + ((languageName == null) ? 0 : languageName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.languageCode.equals(obj)) {
			return true;
		} else {
			return false;
		}
	}
}
