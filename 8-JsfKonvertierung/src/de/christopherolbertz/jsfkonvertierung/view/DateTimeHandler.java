package de.christopherolbertz.jsfkonvertierung.view;

import java.util.Date;

import javax.faces.bean.ManagedBean;

/**
 * Diese Klasse enthält nur eine get-Methode fuer das aktuelle Datum. Sie dient dazu, die Vewendung des DateTime-Konvertierers zu zeigen.
 * @author christopher
 *
 */
@ManagedBean(name ="dtHandler")
public class DateTimeHandler {
	
	/**
	 * Ermittelt das aktuelle Datum.
	 * @return Das aktuelle Datum.
	 */
	public Date getDate() {
		return new Date();
	}
}