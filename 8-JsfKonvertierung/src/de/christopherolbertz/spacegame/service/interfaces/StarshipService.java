package de.christopherolbertz.spacegame.service.interfaces;

import java.util.List;

import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.model.interfaces.Starship;
import de.christopherolbertz.spacegame.model.interfaces.User;

public interface StarshipService {

	/**
	 * Loescht ein Raumschiff aus der Datenbank.
	 * @param object Das zu loeschende Objekt.
	 */
	public abstract void deleteStarship(Starship starship);

	/**
	 * Loescht ein Raumschiff anhand des Primaerschluessels aus der Datenbank.
	 * @param object Der Primaerschluessel des zu loeschenden Objekts.
	 */
	public abstract void deleteStarship(int starshipId);

	/**
	 * Sucht ein Raumschiff anhand ihres Primaerschluessels aus der Datenbank.
	 * @param starshipId Der Primaerschluessel des zu suchenden Raumschiffs.
	 * @return Das gefundene Raumschiff oder null.
	 */
	public abstract Starship findStarship(long starshipId);

	/**
	 * Gibt alle Raumschiffe als Liste aus der Datenbank zurueck.
	 * @return Alle Raumschiffe.
	 */
	public abstract List<Starship> findAllStarships();

	/**
	 * Speichert ein Raumschiff in der Datenbank. Diese Methode erkennt selbststaendig, ob ein Objekt neu in die
	 * Datenbank eingefuegt oder dort aktualisiert werden muss.
	 * @param starship Das zu speichernde Raumschiff.
	 */
	public abstract void saveStarship(Starship starship);
	public abstract void saveStarships(List<Starship> starshipList);
	/**
	 * Sucht alle Raumschiffe auf einem Planeten aus der Datenbank.
	 * @param planet Der Planet.
	 * @return Die Liste mit den Raumschiffen.
	 */
	public List<Starship> findAllStarshipsOnPlanet(Planet planet);
	public void stationStarshipOnPlanet(Starship starship, Planet planet);
	/*public int getMetalCostTransportShip();
	public int getCrystalCostTransportShip();
	public int getMetalCostSpyShip();
	public int getCrystalCostSpyShip();
	public int getMetalCostFrigate();
	public int getCrystalCostFrigate();
	public boolean isStarshipNameAssigend(String starshipName, User user);*/

	List<Starship> findStarshipsByIds(List<String> starshipIds);
}