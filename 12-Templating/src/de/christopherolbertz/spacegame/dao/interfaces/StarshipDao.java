package de.christopherolbertz.spacegame.dao.interfaces;

import java.util.List;

import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.model.interfaces.Starship;

public interface StarshipDao {

	/**
	 * Loescht ein Raumschiff aus der Datenbank.
	 * @param object Das zu loeschende Objekt.
	 * @throws Exception Eine Exception.
	 */
	public abstract void delete(Starship starship);

	/**
	 * Sucht ein Raumschiff anhand seines Primaerschluessels aus der Datenbank.
	 * @param id Der Primaerschluessel des zu suchenden Raumschiff.
	 * @return Das gefundene Raumschiff oder null.
	 */
	public abstract Starship findStarship(Long id);

	/**
	 * Gibt alle Raumschiffe als Liste aus der Datenbank zur�ck.
	 * @return Alle Raumschiffe.
	 */
	public abstract List<Starship> findAllStarships();

	/**
	 * Sucht alle Raumschiffe aus der Datenbank, deren Name dem Suchstring entspricht.
	 * @param searchString Der Suchstring.
	 * @return Alle passenden Raumschiffe.
	 */
	//public abstract List<Starship> getList(String searchString);

	/**
	 * Speichert ein Raumschiff in der Datenbank. Diese Methode erkennt selbstst�ndig, ob ein Objekt neu in die
	 * Datenbank eingefuegt oder dort aktualisiert werden muss.
	 * @param starship Das zu speichernde Raumschiff.
	 */
	public abstract void saveStarship(Starship starship);
	public abstract List<Starship> findAllStarshipsOnPlanet(Planet planet);
	/**
	 * Pr�ft, ob ein Raumschiffname im Reich eines Benutzers bereits vergeben ist. 
	 * @param starshipName Der zu pruefende Name
	 * @param User Der Benutzer.
	 * @return True, wenn der Name vergeben ist, sonst false.
	 */
//public boolean isStarshipNameAssigend(String starshipName, User user);

	public abstract int countStarships();
}