package de.christopherolbertz.spacegame.dao.classes;

import java.util.ArrayList;
import java.util.List;

import de.christopherolbertz.spacegame.dao.interfaces.PlanetDao;
import de.christopherolbertz.spacegame.dao.interfaces.StarshipDao;
import de.christopherolbertz.spacegame.model.classes.FrigateImpl;
import de.christopherolbertz.spacegame.model.classes.SpyShipImpl;
import de.christopherolbertz.spacegame.model.classes.TransportShipImpl;
import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.model.interfaces.Starship;

public class StarshipDaoImpl implements StarshipDao {
	private List<Starship> starshipTable;
	private static PlanetDao planetDao;
	private static StarshipDao starshipDao;
	
	private StarshipDaoImpl() {
		planetDao = PlanetDaoImpl.getInstance();
		initializeStarships();
	}
	
	private void initializeStarships() {
		starshipTable = new ArrayList<>();
		Planet erde = planetDao.findPlanet(1);
		Planet mars = planetDao.findPlanet(6);
		
		Starship enterprise = new FrigateImpl(1, "Enterprise", erde, 100, 30);
		Starship unendlichkeit = new FrigateImpl(2, "Sehnsucht nach Unendlichkeit", erde, 30, 60);
		Starship serenity = new FrigateImpl(3, "Serenity", erde, 80, 40);
		Starship voyager = new FrigateImpl(4, "Voyager", erde, 80, 40);
		Starship defiant = new FrigateImpl(5, "Defiant", mars, 50, 50);
		Starship whiteStar = new FrigateImpl(6, "WhiteStar", mars, 60, 10);
		Starship ds9 = new FrigateImpl(7, "DS9", mars, 80, 70);
		Starship discovery = new FrigateImpl(8, "Discovery", mars, 40, 40);
		
		saveStarship(enterprise);
		saveStarship(unendlichkeit);
		saveStarship(serenity);
		saveStarship(voyager);
		saveStarship(defiant);		
		saveStarship(whiteStar);
		saveStarship(ds9);
		saveStarship(discovery);
		
		erde.landStarship(enterprise);
		erde.landStarship(unendlichkeit);
		erde.landStarship(serenity);
		erde.landStarship(voyager);
		mars.landStarship(defiant);
		mars.landStarship(whiteStar);
		mars.landStarship(ds9);
		mars.landStarship(discovery);
		
		planetDao.savePlanet(erde);
		planetDao.savePlanet(mars);
	}
	
	public static StarshipDao getInstance() {
		if (starshipDao == null) {
			starshipDao = new StarshipDaoImpl();
		}
		
		return starshipDao;
	}
	
	@Override
	public void delete(final Starship starship){
		starshipTable.remove(starship);
	}
	
	@Override
	public Starship findStarship(final Long starshipId) {
		for (final Starship starship: starshipTable) {
			if (starship.getStarshipId() == starshipId) {
				return starship;
			}
		}
		
		return null;
	}
	
	@Override
	public List<Starship> findAllStarships() {
		return starshipTable;
	}	
	
	@Override
	public void saveStarship(final Starship starship) {
		starshipTable.add(starship);
	}
	
	@Override
	public List<Starship> findAllStarshipsOnPlanet(final Planet planet) {
		final List<Starship> starshipList = new ArrayList<Starship>();
		
		for (final Starship starship: starshipTable) {
			if (starship.getCurrentPlanet().equals(planet)) {
				starshipList.add(starship);
			}
		}
		
		return starshipList;
	}

	@Override
	public int countStarships() {
		return starshipTable.size();
	}
}
