package de.christopherolbertz.spacegame.view;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import de.christopherolbertz.spacegame.service.classes.PlanetServiceImpl;
import de.christopherolbertz.spacegame.service.classes.StarshipServiceImpl;
import de.christopherolbertz.spacegame.service.classes.UserServiceImpl;
import de.christopherolbertz.spacegame.service.interfaces.PlanetService;
import de.christopherolbertz.spacegame.service.interfaces.StarshipService;
import de.christopherolbertz.spacegame.service.interfaces.UserService;

@ManagedBean
@ViewScoped
public class StatisticView {
	private StarshipService starshipService;
	private PlanetService planetService;
	private UserService userService;
	
	@PostConstruct
	public void initialize() {
		starshipService = new StarshipServiceImpl();
		planetService = new PlanetServiceImpl();
		userService = new UserServiceImpl();
	}
	
	public int getStarshipCount() {
		return starshipService.countStarships();
	}
	
	public int getUserCount() {
		return userService.countUsers();
	}
	
	public int getPlanetCount() {
		return planetService.countPlanets();
	}
}
