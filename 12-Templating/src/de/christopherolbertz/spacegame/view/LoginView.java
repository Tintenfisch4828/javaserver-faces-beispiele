package de.christopherolbertz.spacegame.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import de.christopherolbertz.spacegame.constants.UrlConstants;
import de.christopherolbertz.spacegame.factories.ServiceFactory;
import de.christopherolbertz.spacegame.model.interfaces.User;
import de.christopherolbertz.spacegame.service.interfaces.UserService;

/**
 * Diese Klasse repraesentiert das Fenster, in welchem der Benutzer sich ins Spiel einloggen kann.
 * @author Christopher
 *
 */

@ManagedBean
@SessionScoped
public class LoginView {
	private List<User> userList;
	private String username;
	private String password;
	private User currentUser;
	private UserService userService;
	
	@PostConstruct
	public void initializeBean() {
		userService = ServiceFactory.createUserService();
		userList = userService.findAllUsers();
	}
	
	public void initialize(ComponentSystemEvent event) {
		username = "";
		password = "";
	}
	
	public String login() {
		for (final User user: userList) {
			if (user.getUsername().equals(username)) {
				currentUser = user;
				return UrlConstants.TACTICAL_OVERVIEW;
			}
		}
		
		return null;
	}
	
	public void logOut(ActionEvent actionEvent) {
		currentUser = null;
	}
	   
	public String toRegisterPage() {
		return "register";
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getUrlIngameLinks() {
		if (isUserLoggedIn()) {
			return UrlConstants.INGAME_LINKS_LOGGED_IN;
		} else {
			return UrlConstants.INGAME_LINKS;
		}
	}

	private boolean isUserLoggedIn() {
		if (currentUser != null) {
			return true;
		} else {
			return false;
		}
	}
	
	public User getCurrentUser() {
		return currentUser;
	}
}
