package de.christopherolbertz.spacegame.model.classes;

import de.christopherolbertz.spacegame.model.interfaces.Frigate;
import de.christopherolbertz.spacegame.model.interfaces.Planet;

public class FrigateImpl extends StarshipImpl implements Frigate {
	private int canonCount;
	private double shields;
	private static final int CRYSTAL_COST = 90;
	private static final int METAL_COST = 180;
	private static final double ATTACK_STRENGTH_FACTOR = 0.6;
	private static final double DEFEND_STRENGTH_FACTOR = 0.5;
	
	public FrigateImpl(String name, Planet currentPlanet, int canonCount, double shields) {
		super(name, currentPlanet);
		this.canonCount = canonCount;
		this.shields = shields;
	}
	
	public FrigateImpl(final int starshipId, final String name, 
			final Planet currentPlanet, final int canonCount, final double shields) {
		super(starshipId, name, currentPlanet);
		this.canonCount = canonCount;
		this.shields = shields;
	}

	@Override
	public double getAttackStrength() {
		return canonCount * ATTACK_STRENGTH_FACTOR;
	}
	
	@Override
	public double getDefendStrength() {
		return shields * DEFEND_STRENGTH_FACTOR;
	}
	
	@Override
	public int getCanonCount() {
		return canonCount;
	}

	@Override
	public void setCanonCount(int canonCount) {
		this.canonCount = canonCount;
	}

	@Override
	public double getShields() {
		return shields;
	}

	public void setShields(double shields) {
		this.shields = shields;
	}

	@Override
	public int getCrystalCost() {
		return CRYSTAL_COST;
	}

	@Override
	public int getMetalCost() {
		return METAL_COST;
	}
}
