package de.christopherolbertz.spacegame.model.interfaces;

import java.util.List;

public interface User {
	public long getUserId();
	public void setUserId(long userId);
	public String getUsername();
	public void setUsername(String username);
	public String getEmailaddress();
	public void setEmailaddress(String emailaddress);
	public String getPassword();
	public void setPassword(String password);
	public boolean isHolidayModus();
	public void setHolidayModus(boolean holidayModus);
	public List<Planet> getMyPlanets();
	public void setMyPlanets(List<Planet> myPlanets);
}
