package de.christopherolbertz.spacegame.constants;

public class UrlConstants {
	public static final String INGAME_LINKS = "ingameLinks.xhtml";
	public static final String INGAME_LINKS_LOGGED_IN = "ingameLinksLoggedIn.xhtml";
	public static final String LANGUAGE_FADE_IN = "language.xhtml";
	public static final String LANGUAGE_FADE_OUT = "languageFadeOut.xhtml";
	public static final String TACTICAL_OVERVIEW = "tacticalOverview";
}
