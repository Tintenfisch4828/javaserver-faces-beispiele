package de.christopherolbertz.spacegame.factories;

import de.christopherolbertz.spacegame.model.classes.FrigateImpl;
import de.christopherolbertz.spacegame.model.classes.PlanetImpl;
import de.christopherolbertz.spacegame.model.classes.SpyShipImpl;
import de.christopherolbertz.spacegame.model.classes.TransportShipImpl;
import de.christopherolbertz.spacegame.model.classes.UserImpl;
import de.christopherolbertz.spacegame.model.interfaces.Planet;
import de.christopherolbertz.spacegame.model.interfaces.Starship;
import de.christopherolbertz.spacegame.model.interfaces.User;
import de.christopherolbertz.spacegame.utils.RandomUtils;

public class ModelFactory {
	public static Starship createTransportShip(final String name, final Planet currentPlanet,
			final int usedSpace, final int maxStockroom) {
		return new TransportShipImpl(name, currentPlanet, usedSpace, maxStockroom);
	}
	
	public static Starship createTransportShip(final String name, final Planet currentPlanet,
			final int maxStockroom) {
		return new TransportShipImpl(name, currentPlanet, 0, maxStockroom);
	}
	
	public static Starship createSpyShip(final int starshipId, final String name, final Planet currentPlanet,
			final int sensorCount, final int camouflage) {
		return new SpyShipImpl(starshipId, name, currentPlanet, sensorCount, camouflage);
	}
	
	public static Starship createFrigate(final int starshipId, final String name, final Planet currentPlanet,
			final int canonCount, final double shields) {
		return new FrigateImpl(starshipId, name, currentPlanet, canonCount, shields);
	}
	
	public static User createUser(final String username, final String emailaddress, final String password) {
		final User user = new UserImpl(username, emailaddress, password);
		return user;
	}
	
	public static Planet createPlanetWithRandomName() {
		String planetName = RandomUtils.createRandomPlanetName();
		return new PlanetImpl(planetName, 0, 0);
	}
}
