package de.christopherolbertz.di.view;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named("viewBean")
@ViewScoped
public class ViewBean implements Serializable {
	public String getText() {
		return "Ich bin die View-Bean";
	}
}
