package de.christopherolbertz.di.view;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import de.christopherolbertz.di.qualifier.MyQualifier;

@MyQualifier
public class ServiceImpl implements Service {

	@Override
	public int getInteger() {
		return 105;
	}
	
}
