package de.christopherolbertz.di.view;

import java.io.Serializable;
import java.util.Date;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;

import de.christopherolbertz.di.qualifier.MyDate;
import de.christopherolbertz.di.qualifier.MyQualifier;
import de.christopherolbertz.di.qualifier.Random;
import de.christopherolbertz.di.qualifier.RandomWithService;


@Named
@Dependent
public class CdiBean implements Serializable {
	@Inject
	private RequestBean requestBean = new RequestBean();
	@Inject
	@MyQualifier
	private Service service;
	@Inject
	private Service anotherService;
	@Inject
	@Named(value = "viewBean")
	private ViewBean viewBean;
	@Inject
	@Random
	private int random;
	@Inject
	@MyDate
	private Date currentDate;
	@Inject
	@RandomWithService
	private int anotherValue;
	
	public int getAnotherValue() {
		return anotherValue;
	}
	
	public void setAnotherValue(int anotherValue) {
		this.anotherValue = anotherValue;
	}
	
	public Date getCurrentDate() {
		return currentDate;
	}
	
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	
	public RequestBean getRequestBean() {
		return requestBean;
	}
	
	public void setRequestBean(RequestBean requestBean) {
		this.requestBean = requestBean;
	}
	
	public String getText() {
		return "Hallo CDI!";
	}
	
	public Service getService() {
		return service;
	}
	
	public void setService(Service service) {
		this.service = service;
	}
	
	public Service getAnotherService() {
		return anotherService;
	}
	
	public void setAnotherService(Service anotherService) {
		this.anotherService = anotherService;
	}
	
	public ViewBean getViewBean() {
		return viewBean;
	}
	
	public void setViewBean(ViewBean viewBean) {
		this.viewBean = viewBean;
	}
	
	public int getRandom() {
		return random;
	}
	
	public void setRandom(int random) {
		this.random = random;
	}
}
