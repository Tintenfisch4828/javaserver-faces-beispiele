package de.christopherolbertz.di.view;

import java.util.Random;

public class RandomService implements Service {

	@Override
	public int getInteger() {
		return new Random().nextInt();
	}

}
