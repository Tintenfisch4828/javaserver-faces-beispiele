package de.christopherolbertz.model.classes;

import de.christopherolbertz.model.interfaces.City;

public class CityImpl implements City {
	private long cityId;
	private String cityName;
	
	public CityImpl(long cityId, String cityName) {
		super();
		this.cityId = cityId;
		this.cityName = cityName;
	}

	@Override
	public long getCityId() {
		return cityId;
	}
	
	@Override
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
	
	@Override
	public String getCityName() {
		return cityName;
	}
	
	@Override
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
}
