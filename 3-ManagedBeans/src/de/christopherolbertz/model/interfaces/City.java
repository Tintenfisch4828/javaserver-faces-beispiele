package de.christopherolbertz.model.interfaces;

public interface City {

	void setCityName(String cityName);

	String getCityName();

	void setCityId(long cityId);

	long getCityId();

}
